import cv2
import os

from keras.models import load_model
import tensorflow as tf

from analysis.evaluation import load_image, predict_to_label_raw, coords2Points
from analysis.predict_refine import expand_point_to_image, refine_predict, load_model_refine


class Cephalometric:
    def __init__(self, raw_model_path=os.path.join(os.path.dirname(__file__), 'checkpoints', 'mobilenet.h5'),
                 refine_model_path=os.path.join(os.path.dirname(__file__), 'checkpoints'), input_raw=720):
        self.graph1 = tf.Graph()
        with self.graph1.as_default():
            self.session1 = tf.Session()
            with self.session1.as_default():
                self.raw_model = load_model(raw_model_path, compile=False)
                self.refine_model = load_model_refine(refine_model_path)
        self.input_raw = input_raw

    def predict(self, img):
        h, w, _ = img.shape
        img_raw, padw, padh, ratio = load_image(img, self.input_raw)
        with self.graph1.as_default():
            with self.session1.as_default():
                pred = self.raw_model.predict(img_raw, batch_size=1)[0] * self.input_raw
        coords = predict_to_label_raw(pred, padw, padh, ratio, [h, w])
        points = coords2Points(coords)
        print(points)
        expand_numbers = [150, 150, 125, 125, 100, 100]
        for expand_number in expand_numbers:
            refine_images = expand_point_to_image(points, img, expand_number)
            with self.graph1.as_default():
                with self.session1.as_default():
                    points = refine_predict(refine_images, self.refine_model, points, expand_number)
        return points


if __name__ == '__main__':
    predictor = Cephalometric()
    img = cv2.imread('/home/mrdung/Documents/dataset/3471833/RawImage/Test1Data/163.bmp')
    print(img.shape)
    print(predictor.predict(img))
