import os
import argparse
import numpy as np


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y


def load_from_txt(file_path):
    print(file_path)
    with open(file_path, 'r') as f:
        data = f.read().split('\n')[:-1]
    points = []
    for i in data:
        if len(i.split(',')) == 2:
            x, y = i.split(',')
            points.append(Point(int(x), int(y)))
    return points


def single_norm(pred, gt):
    return np.sqrt(np.square(pred.x - gt.x) + np.square(pred.y - gt.y))


def file_norm(pred_points, gt_points):
    results = []
    for pred, gt in zip(pred_points, gt_points):
        results.append(single_norm(pred, gt))
    return results


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--pred',
                        default='../result/', help='Path to predict folder')
    parser.add_argument('--gt',
                        default='/data/dungdv/3471833/AnnotationsByMD/400_senior',
                        help='Path to ground true folder')
    args = parser.parse_args()
    total = []
    for file_name in os.listdir(args.pred):
        pred_points = load_from_txt(os.path.join(args.pred, file_name))
        gt_points = load_from_txt(os.path.join(args.gt, file_name))
        total.append(file_norm(pred_points, gt_points))
    total = np.asarray(total)
    x = total <= 20
    acc = np.sum(x, axis=0) / total.shape[0]
    mre = np.sum(total, axis=0) / 150
    mre = np.asarray(mre)
    sd = np.sqrt(np.sum(np.square(np.subtract(total, mre)), axis=0) / (total.shape[0] - 1))
    print(mre)
    print(sd)
    print(acc)
