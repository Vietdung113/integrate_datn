import os
import sys

sys.path.insert(0, '/home/dungdv/sources/datn/')
import argparse
import cv2
from keras.models import load_model
from keras.preprocessing import image
import numpy as np


# from data.gen_data_refine import load_from_txt


def load_from_txt(annotation_path):
    with open(annotation_path, 'r') as f:
        data = f.read().split('\n')[:-1]
    data = [i.split(',') for i in data if len(i.split(',')) == 2]
    return data


def resize_square(img, height=224, color=(0, 0, 0)):  # resize a rectangular image to a padded square
    shape = img.shape[:2]  # shape = [height, width]
    ratio = float(height) / max(shape)  # ratio  = new / nold
    new_shape = [round(shape[0] * ratio), round(shape[1] * ratio)]
    dw = height - new_shape[1]  # width padding
    dh = height - new_shape[0]  # height padding
    top, bottom = dh // 2, dh - (dh // 2)
    left, right = dw // 2, dw - (dw // 2)
    img = cv2.resize(img, (new_shape[1], new_shape[0]), interpolation=cv2.INTER_AREA)  # resized, no border
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color), dw // 2, dh // 2, ratio


def load_image(img, height=224):
    img, padw, padh, ratio = resize_square(img, height=height)
    img_tensor = image.img_to_array(img)
    img_tensor = np.expand_dims(img_tensor, axis=0)
    img_tensor /= 255.0
    return img_tensor, padw, padh, ratio


def predict_to_label_raw(pred, padw, padh, ratio, dims):
    tmp = []
    h, w, c = dims[0], dims[1], 3
    for i, coord in enumerate(pred):
        if i % 2 == 0:
            if int((coord - padw) / ratio) < 0:
                tmp.append(0)
            elif int((coord - padw) / ratio) > w:
                tmp.append(w)
            else:
                tmp.append(int((coord - padw) / ratio))
        else:
            if int((coord - padh) / ratio) < 0:
                tmp.append(0)
            elif int((coord - padh) / ratio) > h:
                tmp.append(h)
            else:
                tmp.append(int((coord - padh) / ratio))
    return tmp


def coords2Points(coords):
    points = []
    for i, coord in enumerate(coords):
        if i % 2 == 0:
            x = int(coord)
        else:
            y = int(coord)
            points.append((x, y))
    return points


def load_model_refine(refine_model):
    models_refine = []
    for i in range(1, 20):
        base_name_file = 'mobilenet_refine_point_{}.h5'.format(i)
        models_refine.append(load_model(os.path.join(refine_model, base_name_file)))
        print('Loaded model: {}'.format(os.path.join(refine_model, base_name_file)))
    return models_refine


def expand(point, images_origin, expand_number=150):
    x, y = point
    # print('Point :{}'.format(point))
    if x < expand_number:
        x_top = 0
        x_bot = x + expand_number
    else:
        x_top = x - expand_number
        x_bot = x + expand_number
    if y < expand_number:
        y_top = 0
        y_bot = y + expand_number
    else:
        y_top = y - expand_number
        y_bot = y + expand_number
    return images_origin[y_top:y_bot, x_top:x_bot]


def expand_point_to_image(points, images_origin, expand_number):
    images = []
    for i, point in enumerate(points):
        images.append(expand(point, images_origin, expand_number))
        # break
    return images


def refine_predict(refine_images, model_refine, points, expand_number):
    result = []
    for img, model, point in zip(refine_images, model_refine, points):
        x, y = point
        h, w, _ = img.shape
        # print(img.shape)
        img_tensor, padw, padh, ratio = load_image(img, 224)
        pred = model.predict(img_tensor, batch_size=1)[0] * 224
        x_pred, y_pred = predict_to_label_raw(pred, padw, padh, ratio, [h, w])
        # print(coords)
        x_pred = int(x_pred) + x - expand_number
        y_pred = int(y_pred) + y - expand_number
        # print('x_pred after refine :{}\t y_pred after refine :{}'.format(x_pred,y_pred))
        result.append((x_pred, y_pred))
        # break

    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--images', help='Path to test image',
                        default='/home/mrdung/Documents/dataset/3471833/RawImage/Test2Data/341.bmp')
    parser.add_argument('--checkpoints', help='Path to checkpoint',
                        default='../checkpoints/mobilenet.h5')
    parser.add_argument('--annotation', help='(Options) path to anntation file',
                        default='/home/mrdung/Documents/dataset/3471833/AnnotationsByMD/400_senior/341.txt')
    parser.add_argument('--refine_model', help='Path to folder contain refine model', default='../checkpoints/')
    args = parser.parse_args()
    model = load_model(args.checkpoints, compile=False)
    print('Loaded predict model ...')
    model_refine = load_model_refine(args.refine_model)
    print('Loaded refine model ...')
    dims = 720
    images_origin = cv2.imread(args.images)
    h, w, c = images_origin.shape
    labels_point = load_from_txt(args.annotation)
    for i, point in enumerate(labels_point):
        print('label point : {}'.format(point))
        cv2.circle(images_origin, (int(point[0]), int(point[1])), 1, (255, 255, 0), 1)
        break

    raw_model_img = images_origin.copy()
    img, padw, padh, ratio = load_image(raw_model_img, height=dims)
    pred = model.predict(img, batch_size=1)[0] * dims
    coords = predict_to_label_raw(pred, padw, padh, ratio, [h, w])

    points = coords2Points(coords)
    for point in points:
        print('original predict :{}'.format(point))
        cv2.circle(images_origin, point, 3, (255, 0, 0), 3)
        break
    refine_images = expand_point_to_image(points, images_origin, 150)
    final_predict = refine_predict(refine_images, model_refine, points, expand_number=150)
    for x in final_predict:
        print(x)
        cv2.circle(images_origin, x, 1, (0, 255, 0), 1)
        break
    images_origin = cv2.resize(images_origin, (800, int(800 * w / h)))
    # cv2.imshow('aaa', images_origin)
    # cv2.waitKey(0)
    # print(final_predict)
    # print(",".join(final_predict))
