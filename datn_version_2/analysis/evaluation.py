import os
import argparse
import cv2
from keras.models import load_model
from keras.preprocessing import image
import numpy as np

from analysis.predict_refine import expand_point_to_image, refine_predict, load_model_refine


def resize_square(img, height=224, color=(0, 255, 0)):  # resize a rectangular image to a padded square
    shape = img.shape[:2]  # shape = [height, width]
    ratio = float(height) / max(shape)  # ratio  = new / nold
    new_shape = [round(shape[0] * ratio), round(shape[1] * ratio)]
    dw = height - new_shape[1]  # width padding
    dh = height - new_shape[0]  # height padding
    top, bottom = dh // 2, dh - (dh // 2)
    left, right = dw // 2, dw - (dw // 2)
    img = cv2.resize(img, (new_shape[1], new_shape[0]), interpolation=cv2.INTER_AREA)  # resized, no border
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT,
                              value=color), dw // 2, dh // 2, ratio


def load_image(img, height=224):
    img, padw, padh, ratio = resize_square(img, height=height)
    img_tensor = image.img_to_array(img)
    img_tensor = np.expand_dims(img_tensor, axis=0)
    img_tensor /= 255.0
    return img_tensor, padw, padh, ratio


def coords2Points(coords):
    points = []
    for i, coord in enumerate(coords):
        if i % 2 == 0:
            x = int(coord)
        else:
            y = int(coord)
            points.append((x, y))
    return points


def predict_to_label_raw(pred, padw, padh, ratio, dims):
    tmp = []
    h, w, c = dims[0], dims[1], 3
    for i, coord in enumerate(pred):
        if i % 2 == 0:
            if int((coord - padw) / ratio) < 0:
                tmp.append(0)
            elif int((coord - padw) / ratio) > w:
                tmp.append(w)
            else:
                tmp.append(int((coord - padw) / ratio))
        else:
            if int((coord - padh) / ratio) < 0:
                tmp.append(0)
            elif int((coord - padh) / ratio) > h:
                tmp.append(h)
            else:
                tmp.append(int((coord - padh) / ratio))
    return tmp


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--test_folder',
                        default='/data/dungdv/3471833/RawImage/Test1Data',
                        help='Path to test folder')
    parser.add_argument('--output', default='../result/',
                        help='Path to output folder file')
    parser.add_argument('--checkpoints',
                        default='../checkpoints/mobilenet.h5',
                        help='Path to checkpoints')
    parser.add_argument('--refine_model', help='Path to refine model', default='../checkpoints/')
    args = parser.parse_args()
    dims = 720
    model = load_model(args.checkpoints, compile=False)
    print('Loaded predict model ...')
    model_refine = load_model_refine(args.refine_model)
    print('Loaded refine model ...')
    if not os.path.exists(args.output):
        os.makedirs(args.output)

    for image_path in os.listdir(args.test_folder):
        image_name = image_path.split('.')[0]
        # print(image_name)
        predict_file_name = image_name + '.txt'
        # print(predict_file_name)
        img = cv2.imread(os.path.join(args.test_folder, image_path))
        h, w, _ = img.shape
        img_raw, padw, padh, ratio = load_image(img, dims)
        pred = model.predict(img_raw, batch_size=1)[0] * dims
        coords = predict_to_label_raw(pred, padw, padh, ratio, [h, w])
        points = coords2Points(coords)
        # print(points)
        expand_numbers = [150, 150, 125, 125, 100, 100]
        for expand_number in expand_numbers:
            refine_images = expand_point_to_image(points, img, expand_number)
            points = refine_predict(refine_images, model_refine, points, expand_number)

        # final_predict = points
        with open(os.path.join(args.output, predict_file_name), 'w') as fout:
            for point in points:
                point = [str(k) for k in point]
                p = ','.join(point)
                fout.write(p + '\n')
