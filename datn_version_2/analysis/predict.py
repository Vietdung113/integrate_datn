import argparse
import cv2
from keras.models import load_model
from keras.preprocessing import image
import numpy as np


def resize_square(img, height=224, color=(0, 0, 0)):  # resize a rectangular image to a padded square
    shape = img.shape[:2]  # shape = [height, width]
    ratio = float(height) / max(shape)  # ratio  = new / nold
    new_shape = [round(shape[0] * ratio), round(shape[1] * ratio)]
    dw = height - new_shape[1]  # width padding
    dh = height - new_shape[0]  # height padding
    top, bottom = dh // 2, dh - (dh // 2)
    left, right = dw // 2, dw - (dw // 2)
    img = cv2.resize(img, (new_shape[1], new_shape[0]), interpolation=cv2.INTER_AREA)  # resized, no border
    return cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color), dw // 2, dh // 2, ratio


def load_image(img, height=224):
    img, padw, padh, ratio = resize_square(img, height=height)
    img_tensor = image.img_to_array(img)
    img_tensor = np.expand_dims(img_tensor, axis=0)
    img_tensor /= 255.0
    return img_tensor, padw, padh, ratio


def coords2Points(coords):
    points = []
    for i, coord in enumerate(coords):
        if i % 2 == 0:
            x = int(coord)
        else:
            y = int(coord)
            points.append((x, y))
    return points


def predict_to_label_raw(pred, padw, padh, ratio, dims):
    tmp = []
    h, w, c = dims[0], dims[1], 3
    for i, coord in enumerate(pred):
        if i % 2 == 0:
            if int((coord - padw) / ratio) < 0:
                tmp.append(0)
            elif int((coord - padw) / ratio) > w:
                tmp.append(w)
            else:
                tmp.append(int((coord - padw) / ratio))
        else:
            if int((coord - padh) / ratio) < 0:
                tmp.append(0)
            elif int((coord - padh) / ratio) > h:
                tmp.append(h)
            else:
                tmp.append(int((coord - padh) / ratio))
    return tmp


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--images', help='Path to test image',
                        default='/home/mrdung/Documents/dataset/3471833/RawImage/Test2Data/341.bmp')
    parser.add_argument('--checkpoints', help='Path to checkpoint',
                        default='/home/mrdung/PycharmProjects/datn/checkpoints/mobilenet.h5')
    parser.add_argument('--annotation', help='(Options) path to anntation file',
                        default='/home/mrdung/Documents/dataset/3471833/AnnotationsByMD/400_senior/341.txt')
    args = parser.parse_args()
    dims = 512
    images_origin = cv2.imread(args.images)
    h, w, c = images_origin.shape
    model = load_model(args.checkpoints, compile=False)
    raw_model_img = images_origin.copy()
    img, padw, padh, ratio = load_image(raw_model_img, height=dims)
    pred = model.predict(img, batch_size=1)[0] * dims
    coords = predict_to_label_raw(pred, padw, padh, ratio, [h, w])
    points = coords2Points(coords)
    for point in points:
        cv2.circle(images_origin, point, 5, (0, 0, 255), 5)
    # annotation
    with open(args.annotation, 'r') as f:
        datas = f.read().split('\n')[:-1]
    for i, point in enumerate(datas):
        point = point.split(',')
        if len(point) == 2:
            cv2.circle(images_origin, (int(point[0]), int(point[1])), 5, (0, 255, 0), 5)

    images_origin = cv2.resize(images_origin, (500, int(500 * h / w)))
    cv2.imwrite('/home/mrdung/Desktop/anh1.bmp', images_origin)
    cv2.imshow('image', images_origin)
    cv2.waitKey(0)
    # coord = (coord - padh) / ratio
    print(points)
    # print(pred)
